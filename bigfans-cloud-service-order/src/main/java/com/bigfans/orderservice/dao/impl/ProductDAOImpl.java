package com.bigfans.orderservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.orderservice.dao.ProductDAO;
import com.bigfans.orderservice.model.Product;
import org.springframework.stereotype.Repository;


/**
 * 
 * @Description:
 * @author lichong
 * 2015年4月4日下午9:27:43
 *
 */
@Repository(ProductDAOImpl.BEAN_NAME)
public class ProductDAOImpl extends MybatisDAOImpl<Product> implements ProductDAO {

	public static final String BEAN_NAME = "productDAO";

}
