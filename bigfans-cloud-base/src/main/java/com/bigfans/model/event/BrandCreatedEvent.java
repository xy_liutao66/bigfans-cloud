package com.bigfans.model.event;

import com.bigfans.framework.event.AbstractEvent;
import lombok.Data;

/**
 * @author lichong
 * @create 2018-02-05 下午8:08
 **/
@Data
public class BrandCreatedEvent extends AbstractEvent{
    protected String id;
    protected String name;
    protected String logo;
    protected String categoryId;
    protected Boolean recommended;
    protected String description;
    protected Integer orderNum;
}
