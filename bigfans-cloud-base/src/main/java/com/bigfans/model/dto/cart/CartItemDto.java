package com.bigfans.model.dto.cart;

import lombok.Data;

/**
 * @author lichong
 * @create 2018-02-24 下午12:27
 **/
@Data
public class CartItemDto {

    private String prodId;
    private Integer quantity;

    public CartItemDto() {
    }

    public CartItemDto(String prodId, Integer quantity) {
        this.prodId = prodId;
        this.quantity = quantity;
    }
}
