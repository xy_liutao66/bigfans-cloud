package com.bigfans.model.pricing.product;

import lombok.Data;

import java.util.List;

@Data
public class ProductPricingDto {

    private List<String> prodIds;

}
