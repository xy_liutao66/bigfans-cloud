package com.bigfans.userservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.userservice.model.OrderCouponAudit;

public interface OrderCouponAuditDAO extends BaseDAO<OrderCouponAudit> {

    OrderCouponAudit getByOrder(String orderId);

}
