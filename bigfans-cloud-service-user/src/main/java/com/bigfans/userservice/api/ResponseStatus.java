package com.bigfans.userservice.api;

/**
 * @author lichong
 * @create 2018-02-17 上午9:46
 **/
public class ResponseStatus {

    public static final Integer UNAUTHORIZED = 4001;
    public static final Integer USER_EXISTS = 4002;
    public static final Integer USER_NOTEXISTS = 4003;
    public static final Integer CODE_INCORRECT = 4004;


}
