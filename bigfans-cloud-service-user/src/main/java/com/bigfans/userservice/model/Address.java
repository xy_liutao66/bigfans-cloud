package com.bigfans.userservice.model;

import com.bigfans.userservice.model.entity.AddressEntity;

/**
 * 
 * @Description:用户地址
 * @author lichong 
 * 2014年12月16日上午10:24:40
 *
 */
public class Address extends AddressEntity {

	private static final long serialVersionUID = 8844228594575134928L;

	public String getDetailAddress() {
		return province + city + region + address;
	}

	@Override
	public String toString() {
		return "Address [consignee=" + consignee + ", email=" + email + ", postalcode=" + postalcode + ", tel=" + tel + ", mobile="
				+ mobile + ", isPrimary=" + isPrimary + ", address=" + address + "]";
	}

}
