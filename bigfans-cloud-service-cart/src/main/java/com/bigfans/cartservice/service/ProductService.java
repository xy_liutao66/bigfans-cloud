package com.bigfans.cartservice.service;

import com.bigfans.cartservice.model.Product;
import com.bigfans.framework.dao.BaseService;

public interface ProductService extends BaseService<Product>{
	
	Product getDetailById(String pid) throws Exception;
	
}
