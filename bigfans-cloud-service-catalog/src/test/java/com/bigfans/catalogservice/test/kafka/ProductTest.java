package com.bigfans.catalogservice.test.kafka;

import com.bigfans.catalogservice.CatalogServiceApp;
import com.bigfans.catalogservice.model.Product;
import com.bigfans.catalogservice.service.product.ProductService;
import com.bigfans.framework.exception.ServiceRuntimeException;
import com.bigfans.framework.kafka.KafkaTemplate;
import com.bigfans.model.event.ProductCreatedEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author lichong
 * @create 2018-03-18 下午3:17
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CatalogServiceApp.class)
public class ProductTest {

    @Autowired
    private KafkaTemplate kafkaTemplate;
    @Autowired
    private ProductService productService;

    @Test
    public void test() throws Exception {
        List<Product> products = productService.listByPgId(null);
        for (Product product: products) {
            ProductCreatedEvent event = new ProductCreatedEvent(product.getId());
            kafkaTemplate.send(event);
        }
    }

    @Test
    public void testPublish() throws Exception{
        ProductCreatedEvent event = new ProductCreatedEvent("1");
        kafkaTemplate.send(event);
    }

}
