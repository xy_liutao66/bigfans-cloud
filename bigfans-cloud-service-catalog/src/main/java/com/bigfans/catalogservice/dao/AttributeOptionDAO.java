package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.AttributeOption;
import com.bigfans.framework.dao.BaseDAO;

import java.util.List;


/**
 * 
 * @Title: 
 * @Description: 
 * @author lichong 
 * @date 2015年10月7日 下午12:39:47 
 * @version V1.0
 */
public interface AttributeOptionDAO extends BaseDAO<AttributeOption> {

	List<AttributeOption> listByCategory(String catId);
	
	List<AttributeOption> listByCategory(List<String> catIds);
	
}
