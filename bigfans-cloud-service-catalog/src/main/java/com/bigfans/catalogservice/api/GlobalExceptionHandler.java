package com.bigfans.catalogservice.api;

import com.bigfans.framework.web.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lichong
 * @create 2018-03-01 下午9:31
 **/
@ControllerAdvice
public class GlobalExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value=Exception.class)
    public RestResponse handleException(Exception exception, HttpServletRequest request) {
        logger.error("系统异常!", exception);
        return RestResponse.error(HttpStatus.INTERNAL_SERVER_ERROR.value() , exception.getMessage());
    }

}
