package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.ProductSpecEntity;
import lombok.Data;

@Data
public class ProductSpec extends ProductSpecEntity {

	private static final long serialVersionUID = -6811834593128832769L;

	protected String option;
	protected Boolean selected;
	protected Boolean selectable;
	protected Boolean outOfStock;
	protected String value;

}
