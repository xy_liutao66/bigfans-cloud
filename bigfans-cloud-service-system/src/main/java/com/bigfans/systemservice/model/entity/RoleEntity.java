package com.bigfans.systemservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Table;

/**
 * @author lichong
 * @create 2018-04-19 下午9:19
 **/
@Data
@Table(name = "Role")
public class RoleEntity extends AbstractModel {

    private String name;

    @Override
    public String getModule() {
        return "Role";
    }
}
