package com.bigfans.searchservice.service.impl;

import com.bigfans.framework.es.ElasticTemplate;
import com.bigfans.framework.es.request.CreateIndexCriteria;
import com.bigfans.framework.es.request.CreateMappingCriteria;
import com.bigfans.framework.es.schema.DefaultIndexSettingsBuilder;
import com.bigfans.searchservice.schema.mapping.ProductMapping;
import com.bigfans.searchservice.service.ProductIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(ProductIndexServiceImpl.BEAN_NAME)
public class ProductIndexServiceImpl implements ProductIndexService{

    public static final String BEAN_NAME = "productIndexService";

    @Autowired
    private ElasticTemplate elasticTemplate;

    public void create() throws Exception {
        this.createIndex();
        this.createMapping();
    }

    public void createIndex() throws Exception {
        elasticTemplate.deleteIndex(ProductMapping.INDEX);
        if (!elasticTemplate.isIndexExists(ProductMapping.INDEX)) {
            CreateIndexCriteria criteria = new CreateIndexCriteria();
            criteria.setIndexName(ProductMapping.INDEX);
            criteria.setAlias(ProductMapping.ALIAS);
            criteria.setVersion(1L);
            criteria.setSettingsBuilder(new DefaultIndexSettingsBuilder());
            elasticTemplate.createIndex(criteria);
        }
    }

    public void createMapping() throws Exception {
        CreateMappingCriteria action = new CreateMappingCriteria();
        action.setMappingBuilder(new ProductMapping());
        action.setIndexName(ProductMapping.INDEX);
        action.setType(ProductMapping.TYPE);
        elasticTemplate.createMapping(action);
    }

}
