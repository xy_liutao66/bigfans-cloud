package com.bigfans.framework.annotations;

@NeedLogin
public @interface NeedRoles {

    String[] roles() default {};

}
