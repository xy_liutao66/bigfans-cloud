package com.bigfans.framework.es;

import com.bigfans.framework.es.request.CreateIndexCriteria;
import com.bigfans.framework.es.schema.DefaultIndexSettingsBuilder;

public class MappingOperator {

	protected ElasticTemplate elasticTemplate;

	public MappingOperator(ElasticTemplate elasticTemplate) {
		this.elasticTemplate = elasticTemplate;
	}
	
	protected boolean createDefaultIndices(String indexName,  String aliasName){
		CreateIndexCriteria criteria = new CreateIndexCriteria();
		criteria.setIndexName(indexName);
		criteria.setAlias(aliasName);
		criteria.setVersion(1L);
		criteria.setSettingsBuilder(new DefaultIndexSettingsBuilder());
		return elasticTemplate.createIndex(criteria);
	}

	protected boolean createMapping() {
		return false;
	}
	
	protected boolean deleteIndex(String indexName){
		return elasticTemplate.deleteIndex(indexName);
	}

}
