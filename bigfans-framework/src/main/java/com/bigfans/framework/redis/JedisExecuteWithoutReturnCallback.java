package com.bigfans.framework.redis;


/**
 * 
 * @Title: 
 * @Description: 
 * @author lichong 
 * @date 2016年1月26日 上午9:29:49 
 * @version V1.0
 */
public interface JedisExecuteWithoutReturnCallback {

	void runInConnection(JedisConnection conn);
	
}
